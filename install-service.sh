#!/bin/bash

cp container-startup.service /etc/systemd/system/
systemctl enable container-startup.service
systemctl start  container-startup.service
systemctl status container-startup.service