# systemd docker compose startup helper
  - `startup.sh` runs `docker compose` for a list of services to bring containers up/down or to pull images.
  - `container-startup.service` is a one-shot systemd service which runs `startup.sh up`
     after boot as soon as the docker service is ready.
     Stop and restart are explicitly not part of the service, as those actions are
     [handled by the docker daemon](https://docs.docker.com/config/containers/start-containers-automatically/).

### Configuration and Setup
  - Copy the template and add absolute paths to your docker-compose.yml files:
    - `cp services.conf.template services.conf`
  - Install the service:
    - `./install-service.sh`
