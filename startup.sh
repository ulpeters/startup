#!/bin/bash

if [ -f /opt/docker/startup/services.conf ]
then
  echo "OK: services.conf file found"
  services=$(cat /opt/docker/startup/services.conf)
else
 echo 'services.conf file is missing, please consult README.md'
 exit 0 
fi

up() {
for service in $services
  do
    cd $service
    docker compose up -d --remove-orphans
  done
}

down() {
for service in $services
  do
    cd $service
    docker compose down
  done
}

pull() {
for service in $services
  do
    cd $service
    docker compose pull
  done
}

case "$1" in
  up)
      up 
      ;;
   
  down)
      down 
      ;;
   
  pull)
      pull 
      ;;
   
  *)
      echo $"Usage: $0 {up|down|pull}"
      exit 1
esac
